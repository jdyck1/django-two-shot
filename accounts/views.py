from django.shortcuts import render
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect


# Create your views here.
def signup_view(request):
    form = UserCreationForm(request.POST)
    if form.is_valid():
        form.save()
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password2"]
        user = authenticate(request, username=username, password=password)
        login(request, user)
        return redirect("home")
    # if form isn't valid do something
    return render(request, "registration/signup.html")
    # return redirect("test")


def test_view(request):
    return render(request, "registration/test.html")
